# Generated by Django 3.2.7 on 2021-10-11 09:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='Message',
            field=models.TextField(),
        ),
    ]
