import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    theme: ThemeData(
      canvasColor: const Color(0xFF1B1D24),
    ),
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<MyApp> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFF3465A4),
          centerTitle: true,
          title: const Text("SIPLASH", style: TextStyle(color: Colors.white)),
        ),
        body: Container(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'assets/logo.png',
                  height: 250,
                  width: 250,
                ),
                const Text(
                  'Login to SIPLASH',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 30),
                ),
                const SizedBox(
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: TextField(
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)),
                          labelText: 'Username',
                          labelStyle: TextStyle(color: Colors.white)),
                    ),
                  ),
                ),
                const SizedBox(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    child: TextField(
                        style: TextStyle(color: Colors.white),
                        obscureText: true,
                        decoration: InputDecoration(
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'Password',
                            labelStyle: TextStyle(color: Colors.white))),
                  ),
                ),
                ElevatedButton(
                    child: const Text("Log In",
                        style: TextStyle(color: Colors.white)),
                    onPressed: () => showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                              title: const Text('Login Successful!',
                                style: TextStyle(
                                  color: Color(0xFF1B1D24))
                              ),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () => Navigator.pop(context, 'OK'),
                                  child: const Text('OK' ,
                                    style: TextStyle(color: Color(0xFF3465A4)),
                                  )
                                )
                              ],
                            )),
                    style: ElevatedButton.styleFrom(
                      primary: const Color(0xFF3465A4),
                    ))
              ],
            )));
  }
}
