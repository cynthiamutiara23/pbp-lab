1. Apakah perbedaan antara JSON dan XML? <br>
    <b>Definisi</b> <br>
    Secara sederhana, JSON (JavaScript Object Notation) adalah format data sedangkan XML (eXtensible Markup Language) adalah bahasa markup. JSON didasarkan pada sintaksis objek JavaScript dan digunakan untuk merepresentasikan data terstruktur/hierarkis dalam format yang dapat dibaca manusia. XML digunakan untuk menyimpan data dan mewakili data terstruktur dalam format yang dapat dibaca bagi mesin maupun manusia. XML lebih susah dibaca dan diinterpretasikan namun lebih aman jika dibandingkan dengan JSON. <br>
    <b>Perbedaan</b>
    - Perbedaan mendasar antara JSON dan XML adalah JSON berorientasi data sedangkan XML berorientasi dokumen.
    - Sintaks JSON lebih ringan dibandingkan XML karena JSON tidak memiliki tag awal dan akhir. XML memerlukan lebih banyak karakter untuk menampilkan data yang sama jika dibandingkan dengan JSON (karena JSON berorientasi data).
    - Tipe data JSON: string, angka, array boolean, dan objek. Tipe data XML: typeless (banyak tipe data seperti angka, teks, gambar, grafik, grafik, dll).
    - JSON tidak bisa menyisipkan comment sedangkan XML bisa menyisipkan comment.

<br>

2. Apakah perbedaan antara HTML dan XML? <br>
    <b>Definisi</b> <br>
    HTML (HyperText Markup Language) adalah bahasa markup yang menampilkan data dan mendeskripsikan struktur halaman web. HTML biasa digunakan untuk membuat halaman web dan aplikasi web. XML (eXtensible Markup Language) juga digunakan untuk membuat halaman web dan aplikasi web tetapi XML adalah bahasa dinamis yang digunakan untuk mengangkut data dan bukan untuk menampilkan data. <br>
    <b>Perbedaan</b>
    - HTML case insensitive, sedangkan XML case sensitive.
    - HTML berfokus untuk menyajikan/menampilkan data, XML berfokus untuk transfer data.
    - HTML: Tag penutup tidak selalu diperlukan (tidak strict). XML: Tag penutup wajib ada (strict).
    - HTML memiliki tag terbatas (sudah ditentukan sebelumnya) sedangkan XML tagnya dapat dikembangkan (tag dapat ditentukan ketika diperlukan).
    - Whitespaces tidak dapat digunakan di HTML, namun dapat digunakan di XML (dalam melakukan coding).
    - HTML tidak menyediakan dukungan namespaces sementara XML menyediakan dukungan namespaces.