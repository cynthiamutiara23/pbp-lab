from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from lab_3.forms import FriendForm

# Create your views here.
@login_required(login_url = "/admin/login")
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)

@login_required(login_url = "/admin/login")
def add_friend(request):
    context ={}
  
    # create object of form
    form = FriendForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if request.method == "POST":
        if form.is_valid():
            # save the form data to model
            form.save()
            return redirect("/lab-3/")
  
    context['form']= form
    return render(request, "lab3_form.html", context)